const fs = require("fs");
const path = require("path");
const app = require("express")();
const request = require("request");
const admZip = require("adm-zip");
const { contextBridge, ipcRenderer } = require("electron");
const { response } = require("express");
const stream = require("stream");
const { promisify } = require("util");
const finished = promisify(stream.finished);

var isDev = process.env.APP_DEV ? process.env.APP_DEV.trim() == "true" : false;

const thirdPServer = "https://windowsdev.t6cloud.com/autoupdater";
const prodAppSource = "../../../../";

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

  next();
});

const unzipFile = (fileName) => {
  const zip = new admZip(fileName);
  if (isDev) {
    zip.extractAllTo("", true);
  } else {
    zip.extractAllTo(path.join(__dirname, prodAppSource), true);
  }
};

const needUpdate = (currentVersion, serverVersion) => {
  console.log(currentVersion, serverVersion);

  const normalizeVersion = (str) => {
    let arr = str.split(/_|\./);
    arr = arr.map((el) => {
      let emptyArr = Array.from("0".repeat(3));
      const elArr = el.split("");
      for (let i = 0; i < elArr.length; i++) {
        emptyArr[emptyArr.length - elArr.length + i] = elArr[i];
      }
      return emptyArr.join("");
    });
    return `1${arr.join("")}`;
  };

  const servVersionInt = parseInt(normalizeVersion(serverVersion));
  const curentVersionInt = parseInt(normalizeVersion(currentVersion));

  if (servVersionInt === curentVersionInt) return 0;

  if (servVersionInt > curentVersionInt) {
    return 1;
  } else {
    return -1;
  }
};

const updateConfigFile = (version) => {
  const v = `v${version.replaceAll(".", "_")}`;
  const fileName = path.join(__dirname, "config.json");
  let rawdata = fs.readFileSync(fileName);
  let data = JSON.parse(rawdata);
  if (isDev) {
    data.app_path_dev = path.join(`./${v}/index.html`);
  } else {
    data.app_path = path.join(prodAppSource, `${v}/index.html`);
  }

  fs.writeFileSync(fileName, JSON.stringify(data, null, 2));
};

app.get("/update", async (req, res) => {
  try {
    const { version } = req.query;

    const fileName = `v${version.replaceAll(".", "_")}.zip`;
    let filePath = "";

    if (isDev) {
      filePath = path.join(__dirname, fileName);
    } else {
      filePath = path.join(__dirname, prodAppSource + fileName);
    }
    let writer = fs.createWriteStream(filePath);
    request(`${thirdPServer}/${fileName}`).pipe(writer);

    await finished(writer);

    unzipFile(filePath);

    updateConfigFile(version);

    return res.send(JSON.stringify({ message: "File Loaded and unziped" }));
  } catch (error) {
    console.log(error);
    return res.send(JSON.stringify({ error: "File load error." }));
  }
});

app.get("/checkUpdate", async (req, res) => {
  try {
    const { version } = req.query;

    const serverVersionPromis = new Promise((resolve, reject) => {
      request(
        `${thirdPServer}/config.json`,
        { json: true },
        (error, res, body) => {
          if (error) {
            reject(error);
          }
          if (!error && res.statusCode == 200) {
            resolve(body);
          }
        }
      );
    });

    const jsonFileConfig = await serverVersionPromis;
    const serverVersion = jsonFileConfig?.version;
    console.log(serverVersion);

    const isNeedUpdate = needUpdate(version, serverVersion);

    if (isNeedUpdate) {
      return res.send(
        JSON.stringify({
          message:
            isNeedUpdate === 1
              ? "New version is available"
              : "Rollback to a previous version",
          version: serverVersion,
        })
      );
    } else {
      return res.send(JSON.stringify({ message: "No updates" }));
    }
  } catch (error) {
    console.log(error);
    return res.send(JSON.stringify({ error: "Check update Error" }));
  }
});

app.listen(8000, () => {
  console.log("Listening at port 8000....");
});

contextBridge?.exposeInMainWorld("electronAPI", {
  setTitle: (title) => ipcRenderer.send("set-title", title),
});
