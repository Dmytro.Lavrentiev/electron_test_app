import "./App.css";
import Image from "./components/Image";
import config from "./config.json";
import axios from "axios";
import { useState } from "react";

function App() {
  const [needUpdate, setNeedUpdate] = useState(false);
  const [serverVersion, setServerVersion] = useState(null);
  const [msg, setMsg] = useState("");
  const version = config.version;

  const updateApp = async () => {
    try {
      const res = await axios.get("http://localhost:8000/update", {
        params: { version: serverVersion.replaceAll(".", "_") },
      });
      const { data } = res;

      window.electronAPI?.setTitle("ok");

      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  const checkUpdate = async () => {
    try {
      const res = await axios.get("http://localhost:8000/checkUpdate", {
        params: { version: version.replaceAll(".", "_") },
      });
      const { data } = res;

      if (data.version) {
        setNeedUpdate(true);
        setServerVersion(data.version);
      }

      setMsg(data.message);

      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div className="App">v{version}</div>

      <Image />
      <div>
        {!needUpdate && <button onClick={checkUpdate}>Check Update</button>}
        {needUpdate && <button onClick={updateApp}>Update</button>}
      </div>
      <div className="msg">{msg}</div>
    </>
  );
}

export default App;
