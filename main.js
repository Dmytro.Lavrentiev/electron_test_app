const { app, BrowserWindow, ipcMain } = require("electron");
const path = require("path");
const fs = require("fs");
var isDev = process.env.APP_DEV ? process.env.APP_DEV.trim() == "true" : false;

function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      preload: path.join(__dirname, "server.js"),
    },
  });

  const fileName = path.join(__dirname, "config.json");
  let rawdata = fs.readFileSync(fileName);
  let data = JSON.parse(rawdata);
  if (isDev) {
    win.loadFile(data.app_path_dev);
  } else {
    win.loadFile(data.app_path);
  }

  win.webContents.openDevTools();
}

function handleReload(event, title) {
  const webContents = event.sender;
  const win = BrowserWindow.fromWebContents(webContents);
  win.setTitle(title);
  app.relaunch();
  app.exit();
}

app.whenReady().then(() => {
  ipcMain.on("set-title", handleReload);
  createWindow();
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
